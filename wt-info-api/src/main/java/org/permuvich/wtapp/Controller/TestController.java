package org.permuvich.wtapp.Controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
public class TestController {

    //Deprecated test request class, remove sometime

    private static final Logger log = LogManager.getLogger(TestController.class);

    @GetMapping(value = "/api/testmessage")
    public String testRequest() {
        log.info("Testrequest just fired!");
        return "Hello world from Spring!";
    }

    @GetMapping(value = "/api/sum/{intOne}/{intTwo}")
    public String testRequest2(@PathVariable Integer intOne, @PathVariable Integer intTwo) {
        return Integer.toString(intOne+intTwo);
    }
}
