package org.permuvich.wtapp.Controller;

import com.fasterxml.jackson.databind.JsonNode;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.permuvich.wtapp.Exception.VehicleRestApiException;
import org.permuvich.wtapp.Service.ScraperService;
import org.permuvich.wtapp.Service.VehicleSearchService;
import org.permuvich.wtapp.Service.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

@CrossOrigin
@RestController
public class VehicleRestApi {
    private static final Logger log = LogManager.getLogger(VehicleRestApi.class);

    @Autowired
    private VehicleService vehicleService;

//    @Autowired
//    private ScraperService scraperService;

    //TODO: Might re-enable this if security between UI and API reworked (as is, anyone could send re-update request)
//    @GetMapping(value = "/vehicles/update")
//    public void updateVehicleData() throws VehicleRestApiException {
//        try {
//            this.scraperService.scrapeVehicles();
//        } catch(IOException ex) {
//            log.error(ex.getStackTrace());
//            throw new VehicleRestApiException(ex.getMessage());
//        }
//    }

    @GetMapping(value = "/vehicles/names")
    public List<String> getAllVehicleNames() throws VehicleRestApiException {
        return vehicleService.getAllVehicleNames();
    }

    @GetMapping(value = "/vehicles", produces = "application/json")
    public List<String> getAllVehicles() throws VehicleRestApiException {
        try {
            return vehicleService.getAllVehicles();
        } catch (IOException ex) {
            ex.printStackTrace();
            throw new VehicleRestApiException(ex.getMessage());
        }
    }

    @GetMapping(value = "/vehicles/vehicle", produces = "application/json")
    public String getVehicleByName(@RequestParam("name") String vehicleName) throws VehicleRestApiException {
        try {
            return vehicleService.getVehicleByName(vehicleName);
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new VehicleRestApiException(ex.getMessage());
        }
    }

    @GetMapping(value = "/vehicles/search", produces = "application/json")
    public String searchVehicleByName(@RequestParam("name") String vehicleName) throws VehicleRestApiException {
        try {
            return vehicleService.searchVehicleByName(vehicleName);
        } catch(Exception ex) {
            ex.printStackTrace();
            throw new VehicleRestApiException(ex.getMessage());
        }
    }



//    @GetMapping(value = "/testvehicle", produces = "application/json")
//    public String getTestVehicle() throws VehicleRestApiException {
//        try {
//            log.info("Got request for testvehicle!");
//            return vehicleService.getTestVehicle();
//        } catch(IOException ex) {
//            log.error(ex.getStackTrace());
//            throw new VehicleRestApiException(ex.getMessage());
//        }
//
//    }
}
