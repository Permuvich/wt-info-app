package org.permuvich.wtapp;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.permuvich.wtapp.Service.ScraperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;

import java.io.IOException;

@SpringBootApplication
public class Application {

    @Autowired
    ScraperService scraperService;

    @Value("${scraper.updateOnStartup}")
    private Boolean scrapeOnStartup;

    private static final Logger log = LogManager.getLogger(Application.class);

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @EventListener(ApplicationReadyEvent.class)
    public void afterStartup() throws IOException {
        log.info("Running afterStartup tasks");
        if(this.scrapeOnStartup) {
            try {
                scraperService.scrapeVehicles();
            } catch(IOException | InterruptedException ex) {
                log.error("Could not scrape vehicle data on startup!");
                log.error(ex.getMessage());
                ex.printStackTrace();
            }
        }
        scraperService.copyDataToBase(); //Throws IOException if data files don't exist
    }
}
