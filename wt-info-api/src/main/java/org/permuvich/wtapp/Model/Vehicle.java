package org.permuvich.wtapp.Model;

import lombok.Data;
import org.hibernate.search.annotations.*;
import org.hibernate.search.annotations.Index;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Data
@Entity
@Indexed
@Table(name = "vehicle")
public class Vehicle {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;

    @Field(index=Index.YES, analyze=Analyze.YES, store=Store.NO)
    @NotNull
    private String name;

    @Field(index=Index.YES, analyze=Analyze.YES, store=Store.NO)
    @NotNull
    private String fullName;

    @Column( length = 1000 )
    private String content;

    protected Vehicle() {}

    public Vehicle(String name, String fullName, String url, String content) {
        this.name = name;
        this.fullName = fullName;
        this.content = content;
    }


}
