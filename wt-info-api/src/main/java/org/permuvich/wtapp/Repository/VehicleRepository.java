package org.permuvich.wtapp.Repository;

import org.permuvich.wtapp.Model.Vehicle;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface VehicleRepository extends JpaRepository<Vehicle, Long> {
    Vehicle findVehicleByName(String name);

    @Query(value = "SELECT name FROM vehicle", nativeQuery = true)
    List<String> findVehicleNames();
}
