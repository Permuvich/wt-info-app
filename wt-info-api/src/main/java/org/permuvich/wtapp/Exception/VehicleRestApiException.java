package org.permuvich.wtapp.Exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR) //Currently only one status, rework
public class VehicleRestApiException extends Exception{
    //TODO: work on exception and http status handling
    public VehicleRestApiException(String message) {
        super(message);
    }
}
