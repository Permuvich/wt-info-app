package org.permuvich.wtapp.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.permuvich.wtapp.Model.Vehicle;
import org.permuvich.wtapp.Repository.VehicleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Service
public class ScraperService {

    // Service to scrape the WT wiki and save vehicle (and weapon) info to local drive in JSON format - for the time being.
    // Scrape only needs to be re-performed on WT updates or if data is lost

    //TODO: Might want to generalize and separate scraper logic from service
    //TODO: redo data persistence, files may not be very suitable

    @Value("${scraper.delay}")
    private Integer delay;
    @Value("${scraper.updateUrls}")
    private Boolean updateUrls;
    @Value("${scraper.updateVehicles}")
    private Boolean updateVehicles;

    @Value("${scraper.vehicles.baseUrl}")
    private String baseUrl;
    @Value("${scraper.vehicles.listUrl}")
    private String listUrl;

    @Value("${scraper.urlFile.path}")
    private String urlFilePath;
    @Value("${scraper.vehicleFile.path}")
    private String vehicleFilePath;
    @Value("${scraper.failedVehicle.path}")
    private String failedVehiclePath;

    @Autowired
    VehicleRepository vehicleRepository;

    ObjectMapper mapper = new ObjectMapper();
    private static final Logger log = LogManager.getLogger(ScraperService.class);

    public void scrapeVehicles() throws IOException, InterruptedException {
        //TODO: loop to get all vehicle page info and save to file
        log.info("Starting scrape for vehicle info");
        mapper.enable(SerializationFeature.INDENT_OUTPUT);

        if(updateUrls) {
            this.scrapeVehicleUrlList();
        }

        if(updateVehicles) {
            File urlFile = new File(urlFilePath);
            if(urlFile.exists() && urlFile.isFile()) {
                HashMap<String, String> urls = mapper.readValue(urlFile, HashMap.class);
                ObjectNode vehicles = mapper.createObjectNode();
                Map<String, String> failedVehicles = new HashMap<>();
                for(String key : urls.keySet()) {
                    JsonNode vehicleNode = this.getVehiclePageInfo(key, urls.get(key), failedVehicles);
                    vehicles.put(key, vehicleNode);
                    TimeUnit.SECONDS.sleep(delay);
                }
                this.saveVehicleJson(vehicles);
                this.saveFailedVehicleJson(failedVehicles);
            }
        }
        log.info("Finished vehicle scrape");
    }

    private JsonNode getVehiclePageInfo(String vehicleName, String pageUrl, Map<String, String> failedVehicles) throws IOException {
        log.info("Getting vehicle info for vehicle ["+vehicleName+"] on url ["+pageUrl+"]");
        Document document;
        ObjectNode result = mapper.createObjectNode();
        try {
            document = Jsoup.connect(pageUrl).get();

            result.put("name", vehicleName);
            result.put("url", pageUrl);
            result.put("fullName", String.join("", document.getElementById("firstHeading").getElementsByTag("span").eachText()));

            Elements params = document.getElementsByClass("flight-parameters");

            // Main parameters (nation, type, rank, br)
            Elements mainParameters = params.get(1).getElementsByTag("td");
            result.put("nation", mainParameters.get(1).text());
            result.put("type", mainParameters.get(3).text());
            result.put("rank", mainParameters.get(5).text());
            Element brValues = mainParameters.get(7).getElementById("mw-customcollapsible-realistic");
            result.put("br", brValues == null ? mainParameters.get(7).text() : brValues.text()); //TODO: Proper arcade/realistic/simulator selection at some point when relevant

            //Characteristics (crew, basic armour)
            Elements characteristics = params.get(2).getElementsByTag("td");
            result.put("crew", characteristics.get(4).text());

            ObjectNode armorNode = mapper.createObjectNode();
            Element armorElement = characteristics.get(6).getElementById("mw-customcollapsible-metric");
            String[] armorValues = armorElement == null ? characteristics.get(6).text().split("/") : armorElement.text().split("/");
            armorNode.put("front", armorValues[0]);
            armorNode.put("sides", armorValues[1]);
            armorNode.put("rear", armorValues[2]);
            armorNode.put("roof", armorValues[3].split(" ")[0]);
            result.put("armor", armorNode);

            //TODO: better armor (hull vs turret etc) from lower on the page (separate section)

            //Statistics (max speed etc)
            //Elements Statistics = params.get(3).getElementsByTag("td");;

            //Images (garage and ammoracks)
            Elements images = document.getElementsByClass("thumbimage");
            //Elements images = document.getElementsByTag("img");

            ObjectNode imageNode = mapper.createObjectNode();
            imageNode.put("garage", baseUrl+images.get(0).attr("src"));
            imageNode.put("ammoRacks", (images.size() > 1 && images.get(1).attr("src").toLowerCase().contains("ammoracks")) ? baseUrl+images.get(1).attr("src") : "");
            result.put("images", imageNode);

            //TODO: more accurate stats, weapons etc

//            log.info(document);
        } catch (Exception ex) {
            log.error("Vehicle "+vehicleName+" not complete, exception: "+ex.getMessage());
            failedVehicles.put(vehicleName, pageUrl);
            ex.printStackTrace();
//            throw new IOException("Could not parse vehicle!");
            //Not throwing since basic info can still be added
        }
        return result;
    }

    private void scrapeVehicleUrlList() throws IOException {
        log.info("Getting vehicle url list from page "+baseUrl+listUrl);
        Document document;
        Map<String, String> result = new HashMap<>();

        document = Jsoup.connect(baseUrl+listUrl).get(); //Not catching exceptions since hard fail is needed if no list found
        Element table = document.getElementsByClass("wikitable").get(0);

        Elements links = table.getElementsByTag("a");
        for(Element link : links) {
            String title = link.attr("title");
            if(!title.contains("Category:")) {
                result.put(link.text(), baseUrl+link.attr("href"));
            }
        }
        saveUrlJson(result);
    }

    private void saveVehicleJson(JsonNode vehicles) throws IOException {
        log.info("Saving vehicle json to file at "+vehicleFilePath);
        //TODO: backup old data file and rethink save paths (SQLlite etc?)
        File f = new File(vehicleFilePath);
        mapper.writeValue(f, vehicles);
    }

    private void saveFailedVehicleJson(Map<String, String> fails) throws IOException {
        log.info("Saving failed vehicles to file at "+failedVehiclePath);
        File f = new File(failedVehiclePath);
        mapper.writeValue(f, fails);
    }

    private void saveUrlJson(Map<String, String> urls) throws IOException {
        //TODO: backup old url file and rethink save paths
        log.info("Saving urls to file at "+urlFilePath);
        File f = new File(urlFilePath);
        mapper.writeValue(f, urls);
    }

    public void copyDataToBase() throws IOException {
        log.info("Saving vehicle entities to database");
        File f = new File(vehicleFilePath);
        if(f.exists() && f.isFile()) {
            JsonNode vehicles = mapper.readTree(f);
            Iterator<Map.Entry<String, JsonNode>> fieldIterator = vehicles.fields();
            while(fieldIterator.hasNext()) {
                Map.Entry<String, JsonNode> entry = fieldIterator.next();
                String name = entry.getKey();
                JsonNode content = entry.getValue();
                Vehicle vehicle = new Vehicle(name, content.get("fullName").toString(), content.get("url").toString(), content.toString());
                vehicleRepository.save(vehicle);
            }
        }
        log.info("Finished database save");
    }
}
