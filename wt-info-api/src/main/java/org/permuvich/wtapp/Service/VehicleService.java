package org.permuvich.wtapp.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.lucene.search.Query;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.FullTextQuery;
import org.hibernate.search.jpa.Search;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.permuvich.wtapp.Model.Vehicle;
import org.permuvich.wtapp.Repository.VehicleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ResourceUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class VehicleService {


    // Service to return vehicle data (provided by the scraper service in the past) based on vehicle names etc.

    @Autowired
    private VehicleRepository vehicleRepository;

    @Autowired
    private VehicleSearchService vehicleSearchService;

    ObjectMapper mapper = new ObjectMapper();

    @Value("${search.vehicleAlias.path}")
    private String vehicleAliasPath;

    HashMap<String, String> vehicleAliases;

    private static final Logger log = LogManager.getLogger(VehicleService.class);

    public List<String> getAllVehicles() throws IOException {
        log.info("Getting all vehicles");
        return vehicleRepository.findAll().stream()
                .map(Vehicle::getContent)
                .collect(Collectors.toList());

    }

    public String getVehicleByName(String vehicleName) throws IOException {
        log.info("Getting vehicle by name "+vehicleName);
        Vehicle vehicle = vehicleRepository.findVehicleByName(vehicleName);
        if(vehicle != null) {
            return vehicle.getContent();
        } else {
            throw new IOException("No vehicle found: "+vehicleName);
        }

    }

    public String searchVehicleByName(String vehicleName) throws InterruptedException {

        List<Vehicle> results = vehicleSearchService.searchVehicleByName(this.getVehicleAlias(vehicleName));
//        List<Vehicle> results = new ArrayList<>();

//        log.info("Found vehicles: ");
//        for(Vehicle result : results) {
//            log.info(result.getName()+" - ["+result.getFullName()+"]");
//        }

        if(results.size() > 0) {
            return results.get(0).getContent();
        } else {
            return "{}"; //TODO: temporary
        }
    }

    public List<String> getAllVehicleNames() {
        return vehicleRepository.findVehicleNames();
    }

    public String getVehicleAlias(String vehicleName) {
        if(this.vehicleAliases == null) {
            try {
                File aliases =  ResourceUtils.getFile("classpath:"+vehicleAliasPath);
                this.vehicleAliases = mapper.readValue(aliases, HashMap.class);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        if(this.vehicleAliases.containsKey(vehicleName)) {
            String alias = this.vehicleAliases.get(vehicleName);
            log.info("Mapped "+vehicleName+" to "+alias);
            return alias;
        } else {
            return vehicleName;
        }
    }
}
