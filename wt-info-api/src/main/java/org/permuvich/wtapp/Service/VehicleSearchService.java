package org.permuvich.wtapp.Service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.lucene.search.Query;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.Search;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.permuvich.wtapp.Model.Vehicle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import java.util.List;

@Service
public class VehicleSearchService {

    private static final Logger log = LogManager.getLogger(VehicleSearchService.class);

    @Autowired
    private final EntityManager entityManager;

    @Autowired
    public VehicleSearchService(EntityManager entityManager) {
        super();
        this.entityManager = entityManager;
    }

    public void initializeVehicleSearch() {
        try {
            FullTextEntityManager fullTextEntityManager = Search.getFullTextEntityManager(this.entityManager);
            fullTextEntityManager.createIndexer(Vehicle.class).startAndWait();
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }

    @Transactional
    public List<Vehicle> searchVehicleByName(String name) {
        log.info("Searching for vehicle by name "+name);

        FullTextEntityManager fullTextEntityManager = Search.getFullTextEntityManager(entityManager);
        QueryBuilder builder = fullTextEntityManager.getSearchFactory().buildQueryBuilder().forEntity(Vehicle.class).get();

        Query luceneQuery = builder.keyword()
                .onFields("name", "fullName")
                .matching(name)
                .createQuery();

        javax.persistence.Query jpaQuery = fullTextEntityManager.createFullTextQuery(luceneQuery, Vehicle.class);

        List<Vehicle> results = null;
        try {
            results = jpaQuery.getResultList();
        } catch(NoResultException nre) {
            log.info("No results for term \""+name+"\"");
        }
        return results;

    }
}
