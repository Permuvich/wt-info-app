package org.permuvich.wtapp.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.persistence.EntityManager;

@EnableAutoConfiguration
@Configuration
public class VehicleSearchConfiguration {

    @Autowired
    private EntityManager entitymanager;

    @Bean
    VehicleSearchService vehicleSearchService() {
        VehicleSearchService vehicleSearchService = new VehicleSearchService(entitymanager);
        vehicleSearchService.initializeVehicleSearch();
        return vehicleSearchService;
    }
}
