import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import {HttpClientModule} from "@angular/common/http";
import {GameApiService} from "./service/game-api.service";
import {AppConfig} from "./shared/app.config";
import {EventParserService} from "./service/event-parser.service";
import {VehicleApiService} from "./service/vehicle-api.service";
import {FormsModule} from "@angular/forms";
import {CookieService} from "ngx-cookie-service";
import {GameVendettaManager} from "./model/game-vendetta.manager";
import {GameEventRowComponent} from "./shared/event-row.component";
import {GameVendettaRowComponent} from "./shared/vendetta-row.component";


@NgModule({
  declarations: [
    AppComponent,
    GameEventRowComponent,
    GameVendettaRowComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    AppConfig,
    GameApiService,
    VehicleApiService,
    EventParserService,
    CookieService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
