import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {GameApiService} from "./service/game-api.service";
import {Observable} from "rxjs/Observable";
import 'rxjs/add/observable/interval';
import {EventParserService} from "./service/event-parser.service";
import {GameEvent} from "./model/game-event";
import {VehicleApiService} from "./service/vehicle-api.service";
import {CookieService} from "ngx-cookie-service";
import {GameVendetta} from "./model/game-vendetta";
import {GameVendettaManager} from "./model/game-vendetta.manager";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  @ViewChild("eventsBox") eventsBox: ElementRef;
  @ViewChild("filteredBox") filteredBox: ElementRef;

  public showFiltered: boolean = true;
  public showEvents: boolean = true;
  public showVendettas: boolean = true;
  public showVehicle: boolean = true;

  title = 'wt-info-app';

  displayList: GameEvent[] = [];
  filteredList: GameEvent[] = [];
  vendettaManager: GameVendettaManager = new GameVendettaManager();

  error = undefined;
  lastEvt = 0;
  lastDmg = 0;

  public tmpPlayerName = undefined;
  public playerName = undefined;
  public tmpGameApiHost = undefined;
  public gameApiHost = undefined;

  //TODO: literally everything worth doing and refactor all of it
  //TODO: move vehicles and other to separate components etc.
  private timerInterval: number = 1000;
  private timerSubscription: any = undefined;

  public displayVehicle: object = undefined;
  public vehicleSearch: string = "Tiger H1";
  public vehicleError = undefined;


  constructor(private gameApi: GameApiService, private vehicleApi: VehicleApiService, private eventParser: EventParserService, private cookieService: CookieService) {}

  ngOnInit(): void {
    // this.setPollGameData(true);
    this.searchVehicleByName();
    this.getMockEvents();
    if(this.cookieService.check("playerFilter")) {
      this.tmpPlayerName = this.cookieService.get("playerFilter");
      this.filterOnName();
    }
    if(this.cookieService.check("gameApiHost")) {
      this.tmpGameApiHost = this.cookieService.get("gameApiHost");
      this.setGameApiHost();
    }
  }

  public searchVehicleByName(vehicleName?: string): void {
    if(this.vehicleSearch) {
      this.vehicleApi.getVehicleByName(vehicleName ? vehicleName : this.vehicleSearch).subscribe((data) => {
        this.displayVehicle = data},
        (error) => this.vehicleError = error, () => this.vehicleError = undefined)
    }

  }

  public setPollGameData(polling: boolean): void {
    if(polling) {
      this.error = false;
      this.timerSubscription = Observable.interval(this.timerInterval).subscribe((t) => { this.onTimerTick() })
    } else {
      if(this.timerSubscription) {
        this.timerSubscription.unsubscribe();
        this.timerSubscription = undefined;
      }
    }
  }

  onTimerTick(): void {
    //Any requests here (events, map info, etc)
    this.getGameEvents();
  }

  //Game events (kill feed, achievements etc)
  //TODO: Separate into proper classes later of course

  public getGameEvents(): void {
      this.gameApi.getGameEvents(this.lastEvt, this.lastDmg, this.gameApiHost).subscribe((events) => {
        this.handleGameEvents(events);
      }, (error) => this.handleFailure(error),
        () => this.handleSuccess());
  }

  handleGameEvents(events: object) {
    if(events) {
      const dmg = events["damage"];
      // const evts = events["events"];

      if(dmg.length > 0) {
        dmg.forEach((evtObj) => {
          let gameEvt = this.eventParser.getGameEvent(evtObj["id"], evtObj["msg"]);
          this.displayList.push(gameEvt);
          if(this.playerName) {

            //Add events to filteredlist
            if(gameEvt.isMessageContainsSubstring(this.playerName)) {
              this.filteredList.push(gameEvt);
            }

            this.vendettaManager.checkEvent(gameEvt, this.playerName);
          }
        });
        this.lastDmg = this.displayList[this.displayList.length - 1].id;
      }
      this.eventsBox.nativeElement.scrollTop = this.eventsBox.nativeElement.scrollHeight;
      this.filteredBox.nativeElement.scrollTop = this.filteredBox.nativeElement.scrollHeight;
      // if(evts.length > 0) {
      //   this.lastEvt = dmg[dmg.length - 1]["id"];
      //   this.displayList.push(...evts);
      //   if(this.playerName) {
      //     this.filteredList.push(...dmg.filter(event => { return event["msg"].indexOf(this.playerName) > 0 }))
      //   }
      // }
    }
    // console.log(this.displayList);
  }

  resetRound(): void {
    this.displayList = [];
    this.filteredList = [];
    this.vendettaManager.reset();
  }

  resetGame(): void {
    this.lastDmg = 0;
    this.lastEvt = 0;
    this.resetRound();
  }

  handleSuccess() {
    this.error = undefined;
  }

  handleFailure(error) {
    // this.setPollGameData(false);
    console.log("Error: ");
    console.log(error);
    // console.log("Stopped game data polling");
    this.error = error;
    this.displayList = [];
  }

  public filterOnName() {
    this.filteredList = [];
    this.playerName = this.tmpPlayerName;
    if(this.playerName) {
      for(let event of this.displayList) {
        if(event.isMessageContainsSubstring(this.playerName)) {
          this.filteredList.push(event);
        }
      }
      //Save cookie
      this.cookieService.set("playerFilter", this.playerName);
    } else {
      this.cookieService.delete("playerFilter");
    }
  }

  public setGameApiHost() {
    this.gameApiHost = this.tmpGameApiHost;
    if(this.gameApiHost) {
      this.cookieService.set("gameApiHost", this.gameApiHost);
    } else {
      this.cookieService.delete("gameApiHost");
    }
  }

  getMockEvents() {
    const mockEvents = ["Player01 (T-34) destroyed Player02 (M4)",
    "Player01 (T-34) has delivered the first strike!" ,
    "Player01 (Pz.IV G) set afire Player02 (ZiS-30)",
    "Player01 (Wirbelwind) damaged Player02 (Me 410 A)",
    "Player01 (Wirbelwind) shot down Player02 (Me 410 A)",
    "Player02 (Me 410 A) has crashed.",
    "Player01 (P40 \"Leoncello\") has been wrecked.",
    "Player01 (Sd.Kfz.234/2) has achieved \"Teamwork: x3\"",
    "Player01 (AC Mk II AA) destroyed Balloon",
    "Player01 (AC Mk II AA) has delivered the final blow!",
    ]

    for(let e of mockEvents) {
      this.displayList.push(this.eventParser.getGameEvent(0, e));
    }

  }
}
