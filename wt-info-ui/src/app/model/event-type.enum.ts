export enum EventType {
  DESTROYED = "destroyed",
  SHOT_DOWN = "shot down",
  SET_AFIRE = "set afire",
  DAMAGED = "damaged",
  HAS_ACHIEVED = "has achieved",
  HAS_CRASHED = "has crashed",
  HAS_BEEN_WRECKED = "has been wrecked",
  FIRST_STRIKE = "has delivered the first strike!",
  FINAL_BLOW = "has delivered the final blow!",
  BALLOON = "destroyed Balloon",
  UNKNOWN = "unknown"
}
