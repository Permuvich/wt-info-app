import {EventType} from "./event-type.enum";

//Holds information needed to check for an event type
export class EventCheck {
  public regExp: RegExp = undefined;
  public eventType: EventType = undefined;

  constructor(regExp: string, eventType: EventType) {
    this.regExp = RegExp(regExp); //Used to determine the type of a message
    this.eventType = eventType;
  }

  public checkMessage(message: string) {
    return this.regExp.exec(message);
  }
}
