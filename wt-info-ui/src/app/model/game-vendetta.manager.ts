import {GameVendetta} from "./game-vendetta";
import {GameEvent} from "./game-event";
import {EventType} from "./event-type.enum";
import {Injectable} from "@angular/core";

//TODO: can move this to a separate component with its own view

export class GameVendettaManager {
  public unResolvedVendettaMap: object = {};

  public fullVendettaList: GameVendetta[] = [];
  public resolvedVendettaList: GameVendetta[] = [];

  //public displayList: GameVendetta[] = []; //keeping separate lists to avoid real-time filtering everywhere
  // public fullNameList: string[] = [];

  checkEvent(event: GameEvent, playerName: string) {
    if(!playerName) return;

    //Check if event concerns any existing vendetta target or player
    let nameList = Object.keys(this.unResolvedVendettaMap);
    let vendettaStarter = (event.isKillEvent() && event.target === playerName);
    let vendettaEnderKill = (event.isKillEvent() && nameList.indexOf(event.target) >= 0);
    let vendettaEnderDie = (event.isDieEvent() && nameList.indexOf(event.subject) >= 0);

    if(!vendettaStarter && !vendettaEnderKill && !vendettaEnderDie) return; //Event does not concern vendettas

    let vendetta: GameVendetta;
    if(vendettaStarter) {
      //Check if existing vendetta with player
      vendetta = this.unResolvedVendettaMap[event.subject];
      if(!vendetta) {
        //Add new vendetta
        vendetta = new GameVendetta();
        vendetta.target = event.subject;
        vendetta.targetVehicle = event.subjectVehicle;
        vendetta.killCount = 1;
        this.unResolvedVendettaMap[event.subject] = vendetta;
      } else {
        vendetta.killCount += 1;
      }
    } else if(vendettaEnderKill) {
      //Finish vendetta because somebody killed them
      vendetta = this.unResolvedVendettaMap[event.target];
      vendetta.resolved = true;
      vendetta.resolvedBy = event.subject;
      vendetta.resolverVehicle = event.subjectVehicle;
      vendetta.resolveEventType = event.eventType;
      delete this.unResolvedVendettaMap[event.target];
      this.resolvedVendettaList.push(vendetta);
    } else if(vendettaEnderDie) {
      //Finish vendetta because they died
      vendetta = this.unResolvedVendettaMap[event.subject];
      vendetta.resolved = true;
      vendetta.resolveEventType = event.eventType;
      delete this.unResolvedVendettaMap[event.subject];
      this.resolvedVendettaList.push(vendetta);
    }
    vendetta.events.push(event);
    this.refreshDisplayList();
  }

  refreshDisplayList() {
    this.fullVendettaList = this.resolvedVendettaList.concat(Object.values(this.unResolvedVendettaMap));
  }

  reset() {
    this.fullVendettaList = [];
    this.resolvedVendettaList = [];
    this.unResolvedVendettaMap = {};
  }
}
