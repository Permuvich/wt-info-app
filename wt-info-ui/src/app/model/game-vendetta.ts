import {GameEvent} from "./game-event";
import {EventType} from "./event-type.enum";

export class GameVendetta {
  public target: string = undefined;
  public targetVehicle: string = undefined;
  public killCount: number = 0;
  public events: GameEvent[] = [];

  public resolved: boolean = false;
  public resolvedBy: string = undefined;
  public resolverVehicle: string = undefined;
  public resolveEventType: EventType = undefined;

  public showDetails: boolean = false;
}
