import {EventParserService} from "../service/event-parser.service";
import {EventType} from "./event-type.enum";

export class GameEvent {

  id: number = undefined;
  message: string = undefined;

  subject: string = undefined;
  subjectVehicle: string = undefined;

  target: string = undefined;
  targetVehicle: string = undefined;
  parsedTime: Date = new Date();

  eventType: EventType;

  isMessageContainsSubstring(subString: string): boolean {
    return (this.message && this.message.indexOf(subString) >= 0);
  }

  isEventConcernsPlayer(playerName: string): boolean {
    return (this.subject === playerName || this.target === playerName);
  }

  isKillEvent(): boolean {
    return (this.eventType === EventType.DESTROYED || this.eventType === EventType.SHOT_DOWN); //I'd actually rather not have eventtypes referenced in too many literal arrays
  }

  isDieEvent(): boolean {
    return (this.eventType === EventType.HAS_BEEN_WRECKED || this.eventType === EventType.HAS_CRASHED);
  }

  isTwoPlayerEvent(): boolean {
    return this.targetVehicle !== undefined;
  }
}
