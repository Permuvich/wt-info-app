import {Injectable} from "@angular/core";
import {GameEvent} from "../model/game-event";
import {EventType} from "../model/event-type.enum";
import {EventCheck} from "../model/event-check";

@Injectable()
export class EventParserService {

  //Mapping the two regexp checks to corresponding event types using EventCheck class
  //At the moment, could also use dataRegexp as typeRegexp, they're separated in case more complicated requirements for capture groups arise

  public actorRegex: string = "([^\\(\\)]+) \\((.+)\\)";

  public eventChecks: EventCheck[] = [
    new EventCheck(this.actorRegex+" destroyed "+this.actorRegex, EventType.DESTROYED),
    new EventCheck( this.actorRegex+" shot down "+this.actorRegex, EventType.SHOT_DOWN),
    new EventCheck( this.actorRegex+" set afire "+this.actorRegex, EventType.SET_AFIRE),
    new EventCheck( this.actorRegex+" damaged "+this.actorRegex, EventType.DAMAGED),
    new EventCheck( this.actorRegex+" has achieved (.+)", EventType.HAS_ACHIEVED),
    new EventCheck( this.actorRegex+" has crashed.*", EventType.HAS_CRASHED),
    new EventCheck( this.actorRegex+" has been wrecked.*", EventType.HAS_BEEN_WRECKED),
    new EventCheck( this.actorRegex+" has delivered the first strike.*", EventType.FIRST_STRIKE),
    new EventCheck( this.actorRegex+" has delivered the final blow.*", EventType.FINAL_BLOW),
    new EventCheck( this.actorRegex+" destroyed Balloon", EventType.BALLOON),
  ];

  public killEvents = [EventType.DESTROYED, EventType.SHOT_DOWN];
  public damageEvents = [EventType.DAMAGED, EventType.SET_AFIRE];

  //TODO: check the format of killing self/abandoning vehicle

  public getGameEvent(id: number, message: string): GameEvent {
    let event = new GameEvent();
    event.id = id;
    event.message = message;

    for(let check of this.eventChecks) {
      let match = check.checkMessage(message);
      if(match) {
        event.eventType = check.eventType;
        event.subject = match[1];
        event.subjectVehicle = match[2];
        if(match[3]) { //TODO: upon adding more types, check if this still applies
          event.target = match[3];
        }
        if(match[4]) { //TODO: same as above
          event.targetVehicle = match[4];
        }
      }
    }

    //If no type matched, must be unknown
    if(!event.eventType) {
      event.eventType = EventType.UNKNOWN;
    }

    return event;
  }

  public getEventType(msg: string): EventType {
    return EventType.UNKNOWN;
  }
}
