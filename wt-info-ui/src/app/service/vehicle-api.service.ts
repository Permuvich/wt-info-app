import {Injectable} from "@angular/core";
import {AppConfig} from "../shared/app.config";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/Observable";

@Injectable()
export class VehicleApiService {
  constructor(private config: AppConfig, private http: HttpClient) {
  }

  getTestVehicle(): Observable<object> {
    console.log("Getting testvehicle from "+this.config.getVehicleApiHost()+"/testvehicle");
    return this.http.get(this.config.getVehicleApiHost()+"/testvehicle")
  }

  getVehicleByName(name: string): Observable<object> {
    console.log("Getting vehicle by name: "+name);

    const urlParams = {name: name.toString()};

    return this.http.get(this.config.getVehicleApiHost()+"/vehicles/search", {params: urlParams});
  }

  // getVehicleNames(): Observable<string[]> {
  //   console.log("Getting vehicle names");
  //
  //   return this.http.get(this.config.getVehicleApiHost()+"/vehicles/names");
  // }
}
