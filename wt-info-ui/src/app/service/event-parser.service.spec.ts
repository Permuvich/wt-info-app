import {EventParserService} from "./event-parser.service";
import {EventType} from "../model/event-type.enum";

describe('EventParserService', () => {

  let eventParser: EventParserService;

  beforeEach( () => {
    eventParser = new EventParserService();
  });
  it("Should get valid GameEvent for destroyed event", () => {
    const msg = getSampleDestroyedEvent();
    const event = eventParser.getGameEvent(0, msg);
    expect(event.id).toBe(0);
    expect(event.message).toBe(msg);
    expect(event.subject).toBe("Player01");
    expect(event.subjectVehicle).toBe("tank01");
    expect(event.target).toBe("=clan= player_02");
    expect(event.targetVehicle).toBe("tank02");
    expect(event.eventType).toBe(EventType.DESTROYED);
  });

  it("Should get valid GameEvent for shot_down event", () => {
    const msg = getSampleShotDownEvent();
    const event = eventParser.getGameEvent(0, msg);
    expect(event.id).toBe(0);
    expect(event.message).toBe(msg);
    expect(event.subject).toBe("Player01");
    expect(event.subjectVehicle).toBe("tank01");
    expect(event.target).toBe("=clan= player_02");
    expect(event.targetVehicle).toBe("tank02");
    expect(event.eventType).toBe(EventType.SHOT_DOWN);
  });

  it("Should get valid GameEvent for set_afire event", () => {
    const msg = getSampleOnFireEvent();
    const event = eventParser.getGameEvent(0, msg);
    expect(event.id).toBe(0);
    expect(event.message).toBe(msg);
    expect(event.subject).toBe("Player01");
    expect(event.subjectVehicle).toBe("tank01");
    expect(event.target).toBe("=clan= player_02");
    expect(event.targetVehicle).toBe("tank02");
    expect(event.eventType).toBe(EventType.SET_AFIRE);
  });

  it("Should get valid GameEvent for damaged event", () => {
    const msg = getSampleDamagedEvent();
    const event = eventParser.getGameEvent(0, msg);
    expect(event.id).toBe(0);
    expect(event.message).toBe(msg);
    expect(event.subject).toBe("Player01");
    expect(event.subjectVehicle).toBe("tank01");
    expect(event.target).toBe("=clan= player_02");
    expect(event.targetVehicle).toBe("tank02");
    expect(event.eventType).toBe(EventType.DAMAGED);
  });

  it("Should get valid GameEvent for achieved event", () => {
    const msg = getSampleAchievedEvent();
    const event = eventParser.getGameEvent(0, msg);
    expect(event.id).toBe(0);
    expect(event.message).toBe(msg);
    expect(event.subject).toBe("Player01");
    expect(event.subjectVehicle).toBe("tank01");
    expect(event.target).toBe("Achievement name");
    expect(event.targetVehicle).toBe(undefined);
    expect(event.eventType).toBe(EventType.HAS_ACHIEVED);
  });

  it("Should get valid GameEvent for unknown event", () => {
    const msg = getSampleUnknownEvent();
    const event = eventParser.getGameEvent(0, msg);
    expect(event.id).toBe(0);
    expect(event.message).toBe(msg);
    expect(event.eventType).toBe(EventType.UNKNOWN);
  })
});

function getSampleDestroyedEvent(): string {
  return "Player01 (tank01) destroyed =clan= player_02 (tank02)";
}

function getSampleShotDownEvent(): string {
  return "Player01 (tank01) shot down =clan= player_02 (tank02)";
}

function getSampleOnFireEvent(): string {
  return "Player01 (tank01) set afire =clan= player_02 (tank02)";
}

function getSampleDamagedEvent(): string {
  return "Player01 (tank01) damaged =clan= player_02 (tank02)";
}

function getSampleAchievedEvent(): string {
  return "Player01 (tank01) has achieved \"Achievement name\"";
}

function getSampleUnknownEvent(): string {
  return "Player01 some random text";
}
