import {Injectable} from "@angular/core";
import {AppConfig} from "../shared/app.config";
import {HttpClient, HttpParams} from "@angular/common/http";
import {Observable} from "rxjs/Observable";

@Injectable()
export class GameApiService{
  constructor(private config: AppConfig, private http: HttpClient) {}

  public getGameEvents(lastEvt: number, lastDmg: number, gameApiHost?: string): Observable<object> {
    const urlParams = {lastEvt: lastEvt.toString(), lastDmg: lastDmg.toString()}

    console.log("Using url "+this.config.getGameApiHost()+"/hudmsg");
    return this.http.get((gameApiHost ? "http://"+gameApiHost+":8111" : this.config.getGameApiHost())+"/hudmsg", {params: urlParams});
  }

  public getGameIsRunning(): boolean {
    return true;
  }
}
