import {Component, EventEmitter, Input, Output} from "@angular/core";
import {GameVendetta} from "../model/game-vendetta";

@Component({
  selector: 'vendetta-row',
  templateUrl: './vendetta-row.component.html',
  styleUrls: ['../app.component.css']
})
export class GameVendettaRowComponent {
  @Input() public gameVendetta: GameVendetta = undefined;
  @Output() public searchVehicleByName: EventEmitter<string> = new EventEmitter();
}
