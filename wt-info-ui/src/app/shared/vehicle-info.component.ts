import {Component, EventEmitter, Input, OnInit, Output} from "@angular/core";
import {GameEvent} from "../model/game-event";

@Component({
  selector: 'vehicle-info',
  templateUrl: './vehicle-info.component.html',
  styleUrls: ['../app.component.css']
})
export class VehicleInfoComponent {
  @Input() public vehicle: object = undefined;
  //@Output() public searchVehicleByName: EventEmitter<string> = new EventEmitter();
}
