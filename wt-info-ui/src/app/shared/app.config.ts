import {environment} from "../../environments/environment";

export class AppConfig{
  public getGameApiHost(){
    return environment.gameApi.host;
  }

  public getVehicleApiHost() {
    return environment.vehicleApi.host;
  }
}
