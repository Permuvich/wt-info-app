import {Component, EventEmitter, Input, OnInit, Output} from "@angular/core";
import {GameEvent} from "../model/game-event";

@Component({
  selector: 'event-row',
  templateUrl: './event-row.component.html',
  styleUrls: ['../app.component.css']
})
export class GameEventRowComponent {
  @Input() public gameEvent: GameEvent = undefined;
  @Output() public searchVehicleByName: EventEmitter<string> = new EventEmitter();
}
