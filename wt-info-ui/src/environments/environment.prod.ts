export const environment = {
  production: true,
  gameApi: {
    host: 'http://127.0.0.1:8111'
  },
  vehicleApi: {
    host: 'http://127.0.0.1:9101'
  }
};
