## WT-info-app

Project contains two modules (readmes in each module directory)
### wt-info-ui
Angular app to parse display game information based on responses from the game API


### wt-info-api
Spring API for scraping and returning vehicle information from the internet etc.

<br>

---

### Known issues
#### General functionality
- Round start and end not automatically detected (game API does not give any indication). 
Could use "last strike" as round end marker, but usually those are not the last events

- Vendetta system does not account for enemies dying before killing player (as can be the case with burning or trading shots). In such a case, the old
vendetta is closed and a new one is immediately started

#### API issues
- Vehicle ingame names sometimes do not map to correct vehicles. These need to be added to the vehicle_aliases.json

#### UI issues
- Some vehicle ingame tags have malformed characters in them, usually target symbols or stars. 
They should be removed if possible, game API already sends malformed text.

---

<br>

### General TODO
- More information in vehicle API (scrape detailed armor, fix up display etc.)
- Rethink UI layout
- Redo UI where possible to be more modular

<br>

- Filter events displayed (on types etc.)
- Round delimiting (save ID start-end for messages of each round, allow history load)
- Copy to clipboard/export